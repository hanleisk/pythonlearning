#海龟模块练习

import turtle

#绘制一个五环
#turtle.width(2)
#turtle.circle(2)


turtle.width(5)
turtle.color("red")
turtle.circle(50)

turtle.penup()
turtle.goto(120,0)
turtle.pendown()
turtle.color("blue")
turtle.circle(50)

turtle.penup()
turtle.goto(240,0)
turtle.pendown()
turtle.color("yellow")
turtle.circle(50)

turtle.penup()
turtle.goto(60,-50)
turtle.pendown()
turtle.color("black")
turtle.circle(50)

turtle.penup()
turtle.goto(180,-50)
turtle.pendown()
turtle.color("purple")
turtle.circle(50)

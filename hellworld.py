def line_seperator():
    print("---------------------------------")


print("hello world")
line_seperator()

# range is a buildin function
for i in range(3):
    print(i)
line_seperator()

print(2 == 1)
print(1 == 1 and 2 == 2 and 3 == 3)
num = 10
if num == 10:
    print("num is 10")
else:
    print("num is not  10")

line_seperator()


# self function test
def mi(x):
    return x ** x


print(mi(3))
print(mi(4))
line_seperator()

# 螺旋线,实时绘制
'''
import turtle
t = turtle.Pen()
for x in range(360):
	t.forward(x)
	t.left(59)
'''

'''
测试多行注释
第二行
'''

# 错误处理

var_input = input("input anything:")

try:
    var_input = int(var_input)
    var_input += 1
    print(var_input)
except (ValueError):
    print("not integer")
except EOFError:
    print("abnormal end of file")


def cal(x):
    print(x * x);


cal(2)
cal(3)
cal(4)
